var cheerio = require("cheerio"),
    request = require("request"),
    agents = require("./agents"),
    S = require('string'),
    _ = require('lodash'),
    mongoskin = require('mongoskin'),
    fivebeans = require('fivebeans');
/**
 * Payload from beanstalks. Demo only
 */
var payload = {
    "baseUrl": "http://www.prothom-alo.com",
    "url": "http://www.prothom-alo.com/international/article/980935/%E0%A6%B8%E0%A7%9C%E0%A6%95%E0%A7%87-%E0%A6%95%E0%A7%81%E0%A6%AA%E0%A6%BF%E0%A7%9F%E0%A7%87-%E0%A6%A4%E0%A6%B0%E0%A7%81%E0%A6%A3%E0%A7%80-%E0%A6%96%E0%A7%81%E0%A6%A8-%E0%A6%AC%E0%A6%BE%E0%A6%81%E0%A6%9A%E0%A6%BE%E0%A6%A4%E0%A7%87-%E0%A6%8F%E0%A6%97%E0%A6%BF%E0%A7%9F%E0%A7%87-%E0%A6%8F%E0%A6%B2%E0%A7%87%E0%A6%A8-%E0%A6%A8%E0%A6%BE-%E0%A6%95%E0%A7%87%E0%A6%89",
    "selectors": {
        "content": ".jw_detail_content_holder",
        "num_likes": "$('.count.mr5').eq(0).text()@eval",
        "date_published": "$('span[itemprop=datePublished]').attr('content')@eval"
    }
};
var crawler_headers = function() {
    return ['User-Agent: ' + agents.list[Math.floor(Math.random() * agents.list.length)]];
}
var db = mongoskin.db('mongodb://localhost:27017/scraper', {
    nativeParser: true,
    strict: false,
    auto_reconnect: true,
    safe: true
});
/**
 * Send Web request
 */
db.bind("sites");
db.sites.find().toArray(function(err, items) {
        console.log(items);
        db.close();
});
request({
    uri: payload.url,
    headers: crawler_headers(),
}, function(error, response, body) {
    if (error) {
        console.warn(error);
        res.writeHead(500);
        res.end(error.message);
    } else {
        var $ = cheerio.load(body);
        /**
         * Payload can have a number of selctors.
         * Get all of them and map keys into mongo
         */
        var output = {
            'url': payload.url
        };
        _.forOwn(payload.selectors, function(selector, key) {
            if (selector.indexOf('@') > -1) {
                var parts = selector.split("@");
                if (parts[1] == "html") {
                    output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                        return $(v).html();
                    })) : $(parts[0]).html();
                } else if (parts[1] == "outerhtml") {
                    output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                        return $.html(v);
                    })) : $.html($(parts[0]));
                } else if (parts[1] == "text") {
                    output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                        return $(v).text();
                    })) : $(parts[0]).text();
                } else if (parts[1] == "eval") {
                    /**
                     * Nasty but no way cheerio can select complex jQuery selectors
                     */
                    output[key] = eval(parts[0]);
                } else {
                    output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                        return $(v).attr(parts[1]);
                    })) : $(parts[0]).attr(parts[1]);
                }
            } else {
                /** If @ is ommited, we just take text value **/
                output[key] = $(selector).text();
            }
        });
        /**
         * Mongo show time
         */
        db.collection("sites").insert(output, function(err, result) {
            if (err) throw err;
            if (result){
                db.close();
            }
        });
    }
});