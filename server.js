var express = require('express'),
    async = require('async'),
    cheerio = require("cheerio"),
    request = require("request").defaults({ maxRedirects: 3 }),
    trim = require("trim"),
    agents = require("./agents"),
    bodyParser = require('body-parser'),
    S = require('string'),
    _ = require('lodash'),
    mongoskin = require('mongoskin'),
    fivebeans = require('fivebeans'),
    app = express();
/**
 * Directives
 */
app.use(bodyParser.urlencoded({
    extended: false
}));
/**
 * CORS
 */
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
var crawler_headers = function() {
    return ['User-Agent: ' + agents.list[Math.floor(Math.random() * agents.list.length)]];
}
var jsonParser = bodyParser.json();
/**
 * Will get links by dom node or array of nodes. Don't need async here. Just Cheerio selector
 * @param url string
 * @param selector array
 */
app.post('/links', jsonParser, function(req, res) {
    /**
     * Multipe links are here. Next Series produce Level 2 Links to scrap
     */
    var client = new fivebeans.client('localhost', 11300);
    async.mapSeries(req.body, function(data, seriesCallback) {
        var base_url = data.url;
        var selectors = data.selectors;
        var links_page = [data.archives_url];
        var links_selector = data.article_links_selector;
        var async_limit = data.limit || 1;
        var query = data.query;
        var $res = res;
        async.mapLimit(links_page, async_limit, function(url, outerCallback) {
            request({
                uri: url,
                headers: crawler_headers(),
            }, function(error, response, body) {
                if (error) {
                    console.warn(error);
                    res.writeHead(500);
                    res.end(error.message);
                } else {
                    $ = cheerio.load(body);
                    var $links = [];
                    /**
                     * There can be multiple selectors for links for a single page
                     * Links are full payload object to feed beanstalkd
                     */
                    $(links_selector).each(function(i, selector) {
                        $(selector).each(function(i, v) {
                            var currentUrl = '';
                            if (v.attribs.href.indexOf(base_url) < 0) {
                                // Relative URLs used
                                currentUrl = base_url + v.attribs.href;
                            } else {
                                // Absolute URLs
                                currentUrl = v.attribs.href;
                            }
                            $links.push({
                                publish_date: data.publish_date,
                                source: data.name,
                                baseUrl: data.url,
                                url: currentUrl,
                                selectors: selectors
                            });
                        });
                    });
                    // $links = $links.slice(0, 5); // For debugging only
                    outerCallback(null, $links);
                }
            });
        }, function(err, items) {
            if (!err) {
                // console.log("Calling Series callback");
                seriesCallback(null, _.flatten(items));
            } else {
                seriesCallback(null, null);
            }
        });
    }, function(err, results) {
        /**
         * We'll add in the Beanstalkd jobs here. The details will be scraped by the workers.
         */
        results = _.flatten(results);
        var $job_count = 0;
        client.on('connect', function() {
            client.use('news-tube', function(err, name) {
                async.each(results, function(link, cb) {
                    var job = {
                        type: 'scrap_pages',
                        payload: link
                    };
                    client.put(1024, 2, 6000, JSON.stringify(['news-tube', job]), function(err, jobid) {
                        $job_count++;
                        if ($job_count >= results.length) {
                            client.end();
                        }
                        //console.log(jobid);
                    });
                }, function(err, result) {
                    if (!err) {
                        // cb();
                    } else {
                        console.log("Error Assiging jobs to server");
                    }
                })
            })
        }).on('error', function(err) {
            console.log(err);
        }).on('close', function() {
            res.setHeader('Content-Type', 'application/json');
            res.json({
                'jobs': results.length
            });
        }).connect();
    });
});
app.get('/browse-news', function(req, res, next) {
    var db = mongoskin.db('mongodb://localhost:27017/scraper', {
        nativeParser: true,
        strict: false,
        auto_reconnect: true,
        safe: true
    });
    async.auto({
        total: function(next) {
            db.collection('sites').count(function(err, count) {
                next(null, count);
            })
        }
    }, function(err, results) {
        if (err) return errorHandler(err);
        var query = req.query;
        db.collection('sites').find().skip(parseInt(query.iDisplayStart, 10)).limit(parseInt(query.iDisplayLength, 10)).toArray(function(err, items) {
            res.json({
                "draw": query.draw,
                "recordsTotal": results.total,
                "recordsFiltered": results.total,
                data: items
            });
        });
    });
});
app.get('/get-log', function(req, res, next) {
    var db = mongoskin.db('mongodb://localhost:27017/scraper', {
        nativeParser: true,
        strict: false,
        auto_reconnect: true,
        safe: true
    });
    async.auto({
        total: function(next) {
            db.collection('sites').count(function(err, count) {
                next(null, count);
            })
        }
    }, function(err, results) {
        if (err) return errorHandler(err);
        var query = req.query;
        db.collection('sites')
            .aggregate(
                [
                    { '$group': { _id: { month: { '$month': "$date" }, day: { '$dayOfMonth': "$date" }, year: { '$year': "$date" } }, source: { '$addToSet': "$source" } } }
                ]
            ,function(err, items) {
                res.json({
                    "draw": query.draw,
                    "recordsTotal": results.total,
                    "recordsFiltered": results.total,
                    data: items
                });
            });
    });
});
app.listen('8081');
console.log('Scraper Started on 8081');
exports = module.exports = app;