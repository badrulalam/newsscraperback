var cheerio = require("cheerio"),
    request = require("request"),
    agents = require("./agents"),
    moment = require('moment'),
    S = require('string'),
    _ = require('lodash'),
    mongoskin = require('mongoskin'),
    fivebeans = require('fivebeans');

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}
var Beanworker = require('fivebeans').worker;
var IndexHandler = function() {
    function IndexHandler() {
        _classCallCheck(this, IndexHandler);
        this.type = "scrap_pages";
    }
    IndexHandler.prototype.work = function work(payload, callback) {
        /**
         * Here's where you do the work. Scrap and dump into Mongo reading the payload
         */
        var crawler_headers = function() {
            return ['User-Agent: ' + agents.list[Math.floor(Math.random() * agents.list.length)]];
        }
        var db = mongoskin.db('mongodb://localhost:27017/scraper', {
            nativeParser: true,
            strict: false,
            auto_reconnect: true,
            safe: true
        });
        /**
         * Send Web request
         */
        db.bind("sites");
        request({
            uri: payload.url,
            headers: crawler_headers(),
        }, function(error, res, body) {
            if (error) {
                console.warn(error);
                // res.writeHead(500);
                res.end(error.message);
            } else {
                var $ = cheerio.load(body);
                /**
                 * Payload can have a number of selctors.
                 * Get all of them and map keys into mongo
                 */                
                var output = {
                    'url': payload.url,
                    'date': moment(payload.publish_date).toDate(),
                    'source' : payload.source
                };
                _.forOwn(payload.selectors, function(selector, key) {
                    if (selector.indexOf('@') > -1) {
                        var parts = selector.split("@");
                        if (parts[1] == "html") {
                            output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                                return $(v).html();
                            })) : $(parts[0]).html();
                        } else if (parts[1] == "outerhtml") {
                            output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                                return $.html(v);
                            })) : $.html($(parts[0]));
                        } else if (parts[1] == "text") {
                            output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                                return $(v).text();
                            })) : $(parts[0]).text();
                        } else if (parts[1] == "eval") {
                            /**
                             * Nasty but no way cheerio can select complex jQuery selectors
                             */
                            output[key] = eval(parts[0]);
                        } else {
                            output[key] = $(parts[0]).length > 1 ? JSON.stringify($(parts[0]).toArray().map(function(v, i) {
                                return $(v).attr(parts[1]);
                            })) : $(parts[0]).attr(parts[1]);
                        }
                    } else {
                        /** If @ is ommited, we just take text value **/
                        output[key] = $(selector).text();
                    }
                });
                /**
                 * Mongo show time
                 */                
                if(!_.isNull(output.title)){
                    db.collection("sites").insert(output, function(err, result) {
                        if (err) throw err;
                        if (result) {
                            console.log("added");
                            db.close();
                        }
                    });                    
                }else{
                    db.close();
                }
            }
        });
        _.delay(function(){
            callback('success');
        },500);
    };
    return IndexHandler;
}();
var handler = new IndexHandler();
var options = {
    id: 'worker_1',
    host: '127.0.0.1',
    port: 11300,
    handlers: {
        'scrap_pages': handler
    },
    ignoreDefault: true
};
var worker = new Beanworker(options);
worker.start(['news-tube']);